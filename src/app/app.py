from flask import Flask, render_template, send_from_directory, send_file, redirect
import os

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 300

@app.route("/")
def path_root(filepath=None):
    return redirect('/index.html')

@app.route("/<path:filepath>")
def path_login(filepath=None):
    if "." not in filepath:
        filepath = f"{filepath}/" if filepath[-1] != "/" else filepath
        filepath += "index.html"

    if os.path.isfile(f"./static/{filepath}"):
        return send_file(f"./static/{filepath}")
    elif os.path.isfile(f"./templates/{filepath}"):
        return render_template(f"/{filepath}", myvar="foobar")

    return f"not found: {filepath}", 404

@app.after_request
def apply_caching(response):
    response.headers["server"] = "Crowdsoft-Template-Engine"
    return response

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8000)
