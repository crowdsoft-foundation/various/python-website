from app import app
import os
import json
import cherrypy

if __name__ == '__main__':
    static_paths = {}
    static_paths[f"/static"] = f"/static/"

    # Mount the application
    cherrypy.tree.graft(app, "/")

    # Add static file serving
    PATH = os.path.abspath(os.path.dirname(__file__))

    def error_page_404(status, message, traceback, version):
        return "Error %s - Well, I'm very sorry but this page doesn't exist!" % status


    cherrypy.config.update({'error_page.404': error_page_404})

    class Static(object):
        pass

    for route, path in static_paths.items():
        cherrypy.tree.mount(Static(), route, config={
            '/': {
                'tools.gzip.on': True,
                'tools.gzip.mime_types': ['text/*', 'application/*', 'font/*'],
                'tools.staticdir.on': True,
                'tools.staticdir.dir': PATH + path,
                'tools.staticdir.index': 'index.html',
                'tools.caching.on': True,
                'tools.caching.force': True,
                'tools.caching.delay': 0,
                'tools.expires.on': True,
                'tools.expires.secs': 3600 * 24 * 365  # expire in an hour
            }
        })


    # Unsubscribe the default server
    cherrypy.server.unsubscribe()

    # Instantiate a new server object
    server = cherrypy._cpserver.Server()

    # Configure the server object
    server.socket_host = "0.0.0.0"
    server.socket_port = 80
    server.thread_pool = 30

    # Subscribe this server
    server.subscribe()

    # Start the server engine (Option 1 *and* 2)
    cherrypy.engine.start()
    cherrypy.engine.block()
