FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

# install pip modules (own filesystem layer)
COPY requirements.txt /tmp/requirements.txt
RUN pip3 install -r /tmp/requirements.txt --no-cache-dir

# copy our app
COPY src /src

WORKDIR "/src/app"

# run our service
RUN chmod 777 /src/runApp.sh && chmod +x /src/runApp.sh
CMD ["sh", "/src/runApp.sh"]